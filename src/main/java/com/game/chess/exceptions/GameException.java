package com.game.chess.exceptions;

public class GameException extends Exception {

    public GameException(String errorMessage) {
        super(errorMessage);
    }

}
