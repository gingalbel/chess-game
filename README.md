# chess-game

To start game, run main() method from ChessApplication class

The game has 2 modes:

1. Read all moves one by one from a file. (See files from "resources" folder). You can add your own file in the resources folder.
2. Read every move from console

######
- Every move is in the format "e2e4", where e2 is the starting position and e4 the destination position.
- All moves must have a piece on the starting square and either an opponent piece or nothing on the destination square. In other cases "Move is invalid" will be displayed.
- Player cannot end their own move in check. He will be asked to do another move.
- If a player starts their move in check a message is displayed
