package com.game.chess.factories;

import com.game.chess.game.GameMode;
import com.game.chess.game.ReadMovesFromConsoleGame;
import com.game.chess.game.ReadMovesFromFileGame;

public class GameModeFactory {

    public static GameMode getGameMode(String type) {
        GameMode game = null;
        switch (type) {
            case "console":
                game = new ReadMovesFromConsoleGame();
                break;
            case "file":
                game = new ReadMovesFromFileGame();
                break;
            default:
                break;
        }
        return game;
    }
}
